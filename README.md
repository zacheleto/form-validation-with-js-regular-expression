# Form validation with regular expression

A simple form where validations in it were created by JavaScript regular expression. 

### Prerequisites

none

### Installing

git clone https://github.com/letowebdev/Form-Validation-with-JS-Regular-Expression.git

## Deployment

https://zacheleto.me/Projects/JS-RE-form-validation

## Built With

* [JavaScript]
* [Regular expressions]
* [Bootstrap]

## Author

* **Zache Abdelatif (Leto)**

## License

This project is licensed under the MIT License
